<?php
class Database
{
    private $hote = 'localhost';
	private $bdd = '';
	private $dbUser = 'root';
	private $dbMdp = '';
	private $port = "3306";

    private static $pdo = null;
    public static function connect()
    {
        try
        {
            self::$pdo = new PDO('mysql:host='.$hote.';port='.$port.';dbname='.$bdd, $dbUser, $dbMdp);
        }
        catch (PDOException $e)
        {
            die($e-> getMessage());
            exit();
        }
        return self::$pdo;
    }

    public static function disconnect()
    {
        self::$pdo = null;
    }
}