﻿#coding:utf-8
import os as o
###############################
## Autor : Bengi40
##
## Date : 23/03/2023
##
## Version : v1.0
###############################

class College:

    def __init__(self):
        pass
        
    def _setWhereIs(self):
       return o.getenv("logonserver")
    
    def _setCodeRNE(self):
        return self.whereIs[5:len(self.whereIs)]
        
    def _setAppServer(self):
        return "app-{}".format(self.codeRNE)
    
    def _setDcServer(self):
        return "dc-{}".format(self.codeRNE)
    
    def _setDocnServer(self):
        return "\\\\app-{}\Docs".format(self.codeRNE)
    
    def _setDocnServer(self):
        return "\\\\app-{}\Docs".format(self.codeRNE)
    
    
    ###############################
    ## Définission des Properties
    ###############################
    whereIs = property(_setWhereIs)
    codeRNE = property(_setCodeRNE)
    appServer = property(_setAppServer)
    dcServer = property(_setDcServer)
    docnServer = property(_setDocnServer)


    ###############################
    ## Méthodes
    ###############################

    #Affichage des Info
    def getInfo(self):
        print("\tCode RNE : {} \n".format(self.codeRNE))
        print("\t## Serveur du collège ##")
        print("\tServeur App : {} \n".format(self.appServer))
        print("\tServeur DC : {} \n".format(self.dcServer))

        print("\t## Dossier DocsN ##")
        print("\tServeur Docs N : {}\n".format(self.docnServer))
    
    # Vérification Existance dossier
    def verificationDossier(dossier):
        if o.path.exists(dossier):
            print("le dossier existe")
            return True
        else:
            print("le dossier n'existe pas")
            return False

    # #Création Arborescence
    # def arborescenceNumerisation():
    #     verification = verificationDossier(docnNumerisation)
    #     if not verification:
    #         o.mkdir(docnNumerisation)

    #     o.mkdir(docnNumerisation + "\\Déploiement")
    #     o.mkdir(docnNumerisation + "\\Déploiement\\EIM")
    #     o.mkdir(docnNumerisation + "\\Déploiement\\Fixe")
    #     o.mkdir(docnNumerisation + "\\Déploiement\\Stock")

    #     o.mkdir(docnNumerisation + "\\Factures payées")

    #     o.mkdir(docnNumerisation + "\\Fiche états")

    #     o.mkdir(docnNumerisation + "\\Incidents")
    #     o.mkdir(docnNumerisation + "\\Incidents\\Housses")
    #     o.mkdir(docnNumerisation + "\\Incidents\\Chargeurs")
    
    
