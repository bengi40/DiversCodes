<?php
//Recupère les valeurs entrés dans un formulaire et vérifie qu'il y a pas de caractère spéciaux, 
// d'espace... puis renvoi les données nettoyées

    function checkInput($data) 
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>