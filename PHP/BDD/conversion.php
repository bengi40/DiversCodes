<?php
//Conversion date US en date FR
function dateFr($dateUs){
	$date = explode("-",$dateUs); //sépare les différentes partie délimité par des - => devient un array
	$nouvelleDate = $date[2].'/'.$date[1].'/'.$date[0]; // nous mettons dans les entrées du array dans l'ordre FR
	return $nouvelleDate; // renvoi de la date format FR
}
?>