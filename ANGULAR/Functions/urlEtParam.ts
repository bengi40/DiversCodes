// Fonction pour la récupération de l'url
private getUrlLikeRouter(): string {
	if (this.router && this.router.routerState && this.router.routerState.snapshot) {
		return this.router.routerState.snapshot.url;
	} else if (this.router) {
		return this.router.url;
	} else {
		return null;
	}
}


//Fonction Split pour la récupération de paramètre dans une URL
// type d'URL : localhost:8888/spe?id=1&id=2
getParamUrl(psParamName: string): Array<string> {
	const valueOfParam = [];
	//on découpe dans un premier temps par le ?
	const tabUrl = this.getUrlLikeRouter().split('?');
	if (tabUrl.length > 1) {
		// On découpe par & afin de séparer les paramètres
		const tabParams = tabUrl[1].split('&');
		// on parcours tous les paramètres
		tabParams.forEach(param => {
			// On découpe par = pour récupérer la valeur du paramètre
			const keyVal = param.split('=');
			if (keyVal.length > 1 && keyVal[0] === psParamName) {
				valueOfParam.push(keyVal[1]);
			}
		});
	}
	return valueOfParam;
}
