#!/bin/bash

nameSite=$1
pathSave=$2
dateJour=$3

#Bdd
host=$4
user=$5
mdp=$6
bdd=$7

sautLigne="-----------------------------"

function hasFolder
{
    if [ ! -d $pathSave"/"$nameSite ];then
        mkdir -p $pathSave"/"$nameSite
        if [ $? -eq 0 ]; then
            echo $dateJour "Dossier '$nameSite' est créé"  >> $pathSave$nameSite/fichier.log
            echo $sautLigne >> $pathSave$nameSite/fichier.log
        else
            echo $dateJour "Erreur lors de la création du dossier '$nameSite'" >> $pathSave$nameSite/fichier.log
            echo $sautLigne >> $pathSave$nameSite/fichier.log
        fi
    else
        echo $dateJour "Le dossier '$nameSite'  exite déjà"  >> $pathSave$nameSite/fichier.log
        echo $sautLigne >> $pathSave$nameSite/fichier.log
    fi
}

hasFolder

if [ $? -eq 0 ]; then
    mysqldump --no-tablespaces --host=$host --user=$user --password=$mdp $bdd >> $pathSave$nameSite/$bdd-$dateJour.sql
    
    if [ $? -eq 0 ];then
        echo $dateJour "Sauvegarde de la base de donnée" $bdd "a bien été sauvegardée" >> $pathSave$nameSite/fichier.log
        echo $sautLigne >> $pathSave$nameSite/fichier.log
    else
        echo $dateJour "Erreur lors de la sauvegarde de la base de donnée" $bdd >> $pathSave$nameSite/fichier.log
        echo $sautLigne >> $pathSave$nameSite/fichier.log
    fi
fi


