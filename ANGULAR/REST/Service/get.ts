/**
*Environment.urlBack => mettre l'url du back
*/
getListe(): Promise<Array<Spe>> {
    return this.http.get<Array<Spe>>(environment.urlBack + '/rest/spes').toPromise()
    .then(spes => {
        const spesToReturned = [];
        if (spes && spes.length) {
            spes.forEach(spe => {
                spesToReturned.push(new Spe(spe));
            });
            return spesToReturned;
        }
    });
}